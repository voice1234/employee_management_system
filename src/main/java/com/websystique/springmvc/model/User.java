package com.websystique.springmvc.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="APP_USER")
public class User implements Serializable{

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@NotEmpty
	@Column(name="SSO_ID", unique=true, nullable=false)
	private String ssoId;
	
	@Column(name="PASSWORD")
	private String password;
		
	@NotEmpty
	@Column(name="FIRST_NAME", nullable=false)
	private String firstName;

	@NotEmpty
	@Column(name="LAST_NAME", nullable=false)
	private String lastName;

	@NotEmpty
	@Column(name="EmpID", nullable=false)
	private String EmpID;
	@NotEmpty
	@Column(name="DateOfJoining", nullable=false)
	private String DateOfJoining; 
	@NotEmpty
	@Column(name="Band", nullable=false)
	private String Band;
	@NotEmpty
	@Column(name="Designation", nullable=false)
	private String Designation;
	@NotEmpty
	@Column(name="Gender", nullable=false)
	private String Gender;
	@NotEmpty
	@Column(name="ExtensionNo", nullable=false)
	private String ExtensionNo;
	
	
	@NotEmpty
	@Column(name="MobileNumber", nullable=false)
	private String MobileNumber;
	@NotEmpty
	@Column(name="EmergencyContactNo", nullable=false)
	private String EmergencyContactNo;
	@NotEmpty
	@Column(name="PersonalEmailID", nullable=false)
	private String PersonalEmailID;
	
	@NotEmpty
	@Column(name="BloodGroup", nullable=false)
	private String BloodGroup;
	@NotEmpty
	@Column(name="DateOfBirthRecords", nullable=false)
	private String DateOfBirthRecords;
	@NotEmpty
	@Column(name="DateOfBirthActual", nullable=false)
	private String DateOfBirthActual;
	
	@NotEmpty
	@Column(name="MaritalStatus", nullable=false)
	private String MaritalStatus;
	@NotEmpty
	@Column(name="SpouseName", nullable=false)
	private String SpouseName;
	@NotEmpty
	@Column(name="FatherName", nullable=false)
	private String FatherName;
	@NotEmpty
	@Column(name="MotherName", nullable=false)
	private String MotherName;
	@NotEmpty
	@Column(name="AadhaarNumber", nullable=false)
	private String AadhaarNumber;
	@NotEmpty
	@Column(name="PassportNo", nullable=false)
	private String PassportNo;
	@NotEmpty
	@Column(name="PassportIssuedDate", nullable=false)
	private String PassportIssuedDate;
	@NotEmpty
	@Column(name="PassportExpiryDate", nullable=false)
	private String PassportExpiryDate;
	@NotEmpty
	@Column(name="PreviousExperience", nullable=false)
	private String PreviousExperience;
	@NotEmpty
	@Column(name="PermanentAddress", nullable=false)
	private String PermanentAddress;
	@NotEmpty
	@Column(name="PresentAddress", nullable=false)
	private String PresentAddress;
	@NotEmpty
	@Column(name="CurrentProjectManager", nullable=false)
	private String CurrentProjectManager;
	@NotEmpty
	@Column(name="CurrentProject", nullable=false)
	private String CurrentProject;
	@NotEmpty
	@Column(name="CurrentReportingManager1", nullable=false)
	private String CurrentReportingManager1;
	@NotEmpty
	@Column(name="HRBP", nullable=false)
	private String HRBP;
	@NotEmpty
	@Column(name="CurrentProjectLead", nullable=false)
	private String CurrentProjectLead;
	@NotEmpty
	@Column(name="CurrentWorkLocation", nullable=false)
	private String CurrentWorkLocation;
	@NotEmpty
	@Column(name="Graduation", nullable=false)
	private String Graduation;
	@NotEmpty
	@Column(name="PostGraduation", nullable=false)
	private String PostGraduation;	
	@NotEmpty
	@Column(name="Others", nullable=false)
	private String Others;
	@NotEmpty
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "APP_USER_USER_PROFILE", 
             joinColumns = { @JoinColumn(name = "USER_ID") }, 
             inverseJoinColumns = { @JoinColumn(name = "USER_PROFILE_ID") })
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();
	public String getEmpID() {
		return EmpID;
	}

	public void setEmpID(String empID) {
		EmpID = empID;
	}

	public String getDateOfJoining() {
		return DateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		DateOfJoining = dateOfJoining;
	}

	public String getBand() {
		return Band;
	}

	public void setBand(String band) {
		Band = band;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getExtensionNo() {
		return ExtensionNo;
	}

	public void setExtensionNo(String extensionNo) {
		ExtensionNo = extensionNo;
	}
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		password="dflt";
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getEmergencyContactNo() {
		return EmergencyContactNo;
	}

	public void setEmergencyContactNo(String emergencyContactNo) {
		EmergencyContactNo = emergencyContactNo;
	}

	public String getPersonalEmailID() {
		return PersonalEmailID;
	}

	public void setPersonalEmailID(String personalEmailID) {
		PersonalEmailID = personalEmailID;
	}

	public String getBloodGroup() {
		return BloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		BloodGroup = bloodGroup;
	}

	public String getDateOfBirthRecords() {
		return DateOfBirthRecords;
	}

	public void setDateOfBirthRecords(String dateOfBirthRecords) {
		DateOfBirthRecords = dateOfBirthRecords;
	}

	public String getDateOfBirthActual() {
		return DateOfBirthActual;
	}

	public void setDateOfBirthActual(String dateOfBirthActual) {
		DateOfBirthActual = dateOfBirthActual;
	}

	public String getMaritalStatus() {
		return MaritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		MaritalStatus = maritalStatus;
	}

	public String getSpouseName() {
		return SpouseName;
	}

	public void setSpouseName(String spouseName) {
		SpouseName = spouseName;
	}

	public String getFatherName() {
		return FatherName;
	}

	public void setFatherName(String fatherName) {
		FatherName = fatherName;
	}

	public String getMotherName() {
		return MotherName;
	}

	public void setMotherName(String motherName) {
		MotherName = motherName;
	}

	public String getAadhaarNumber() {
		return AadhaarNumber;
	}

	public void setAadhaarNumber(String aadhaarNumber) {
		AadhaarNumber = aadhaarNumber;
	}

	public String getPassportNo() {
		return PassportNo;
	}

	public void setPassportNo(String passportNo) {
		PassportNo = passportNo;
	}

	public String getPassportIssuedDate() {
		return PassportIssuedDate;
	}

	public void setPassportIssuedDate(String passportIssuedDate) {
		PassportIssuedDate = passportIssuedDate;
	}

	public String getPassportExpiryDate() {
		return PassportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		PassportExpiryDate = passportExpiryDate;
	}

	public String getPreviousExperience() {
		return PreviousExperience;
	}

	public void setPreviousExperience(String previousExperience) {
		PreviousExperience = previousExperience;
	}

	public String getPermanentAddress() {
		return PermanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		PermanentAddress = permanentAddress;
	}

	public String getPresentAddress() {
		return PresentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		PresentAddress = presentAddress;
	}

	public String getCurrentProjectManager() {
		return CurrentProjectManager;
	}

	public void setCurrentProjectManager(String currentProjectManager) {
		CurrentProjectManager = currentProjectManager;
	}

	public String getCurrentProject() {
		return CurrentProject;
	}

	public void setCurrentProject(String currentProject) {
		CurrentProject = currentProject;
	}

	public String getCurrentReportingManager1() {
		return CurrentReportingManager1;
	}

	public void setCurrentReportingManager1(String currentReportingManager1) {
		CurrentReportingManager1 = currentReportingManager1;
	}

	public String getHRBP() {
		return HRBP;
	}

	public void setHRBP(String hRBP) {
		HRBP = hRBP;
	}

	public String getCurrentProjectLead() {
		return CurrentProjectLead;
	}

	public void setCurrentProjectLead(String currentProjectLead) {
		CurrentProjectLead = currentProjectLead;
	}

	public String getCurrentWorkLocation() {
		return CurrentWorkLocation;
	}

	public void setCurrentWorkLocation(String currentWorkLocation) {
		CurrentWorkLocation = currentWorkLocation;
	}

	public String getGraduation() {
		return Graduation;
	}

	public void setGraduation(String graduation) {
		Graduation = graduation;
	}

	public String getPostGraduation() {
		return PostGraduation;
	}

	public void setPostGraduation(String postGraduation) {
		PostGraduation = postGraduation;
	}

	public String getOthers() {
		return Others;
	}

	public void setOthers(String others) {
		Others = others;
	}

	public void setUserProfiles(Set<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((AadhaarNumber == null) ? 0 : AadhaarNumber.hashCode());
		result = prime * result + ((Band == null) ? 0 : Band.hashCode());
		result = prime * result + ((BloodGroup == null) ? 0 : BloodGroup.hashCode());
		result = prime * result + ((CurrentProject == null) ? 0 : CurrentProject.hashCode());
		result = prime * result + ((CurrentProjectLead == null) ? 0 : CurrentProjectLead.hashCode());
		result = prime * result + ((CurrentProjectManager == null) ? 0 : CurrentProjectManager.hashCode());
		result = prime * result + ((CurrentReportingManager1 == null) ? 0 : CurrentReportingManager1.hashCode());
		result = prime * result + ((CurrentWorkLocation == null) ? 0 : CurrentWorkLocation.hashCode());
		result = prime * result + ((DateOfBirthActual == null) ? 0 : DateOfBirthActual.hashCode());
		result = prime * result + ((DateOfBirthRecords == null) ? 0 : DateOfBirthRecords.hashCode());
		result = prime * result + ((DateOfJoining == null) ? 0 : DateOfJoining.hashCode());
		result = prime * result + ((Designation == null) ? 0 : Designation.hashCode());
		result = prime * result + ((EmergencyContactNo == null) ? 0 : EmergencyContactNo.hashCode());
		result = prime * result + ((EmpID == null) ? 0 : EmpID.hashCode());
		result = prime * result + ((ExtensionNo == null) ? 0 : ExtensionNo.hashCode());
		result = prime * result + ((FatherName == null) ? 0 : FatherName.hashCode());
		result = prime * result + ((Gender == null) ? 0 : Gender.hashCode());
		result = prime * result + ((Graduation == null) ? 0 : Graduation.hashCode());
		result = prime * result + ((HRBP == null) ? 0 : HRBP.hashCode());
		result = prime * result + ((MaritalStatus == null) ? 0 : MaritalStatus.hashCode());
		result = prime * result + ((MobileNumber == null) ? 0 : MobileNumber.hashCode());
		result = prime * result + ((MotherName == null) ? 0 : MotherName.hashCode());
		result = prime * result + ((Others == null) ? 0 : Others.hashCode());
		result = prime * result + ((PassportExpiryDate == null) ? 0 : PassportExpiryDate.hashCode());
		result = prime * result + ((PassportIssuedDate == null) ? 0 : PassportIssuedDate.hashCode());
		result = prime * result + ((PassportNo == null) ? 0 : PassportNo.hashCode());
		result = prime * result + ((PermanentAddress == null) ? 0 : PermanentAddress.hashCode());
		result = prime * result + ((PersonalEmailID == null) ? 0 : PersonalEmailID.hashCode());
		result = prime * result + ((PostGraduation == null) ? 0 : PostGraduation.hashCode());
		result = prime * result + ((PresentAddress == null) ? 0 : PresentAddress.hashCode());
		result = prime * result + ((PreviousExperience == null) ? 0 : PreviousExperience.hashCode());
		result = prime * result + ((SpouseName == null) ? 0 : SpouseName.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((ssoId == null) ? 0 : ssoId.hashCode());
		result = prime * result + ((userProfiles == null) ? 0 : userProfiles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (AadhaarNumber == null) {
			if (other.AadhaarNumber != null)
				return false;
		} else if (!AadhaarNumber.equals(other.AadhaarNumber))
			return false;
		if (Band == null) {
			if (other.Band != null)
				return false;
		} else if (!Band.equals(other.Band))
			return false;
		if (BloodGroup == null) {
			if (other.BloodGroup != null)
				return false;
		} else if (!BloodGroup.equals(other.BloodGroup))
			return false;
		if (CurrentProject == null) {
			if (other.CurrentProject != null)
				return false;
		} else if (!CurrentProject.equals(other.CurrentProject))
			return false;
		if (CurrentProjectLead == null) {
			if (other.CurrentProjectLead != null)
				return false;
		} else if (!CurrentProjectLead.equals(other.CurrentProjectLead))
			return false;
		if (CurrentProjectManager == null) {
			if (other.CurrentProjectManager != null)
				return false;
		} else if (!CurrentProjectManager.equals(other.CurrentProjectManager))
			return false;
		if (CurrentReportingManager1 == null) {
			if (other.CurrentReportingManager1 != null)
				return false;
		} else if (!CurrentReportingManager1.equals(other.CurrentReportingManager1))
			return false;
		if (CurrentWorkLocation == null) {
			if (other.CurrentWorkLocation != null)
				return false;
		} else if (!CurrentWorkLocation.equals(other.CurrentWorkLocation))
			return false;
		if (DateOfBirthActual == null) {
			if (other.DateOfBirthActual != null)
				return false;
		} else if (!DateOfBirthActual.equals(other.DateOfBirthActual))
			return false;
		if (DateOfBirthRecords == null) {
			if (other.DateOfBirthRecords != null)
				return false;
		} else if (!DateOfBirthRecords.equals(other.DateOfBirthRecords))
			return false;
		if (DateOfJoining == null) {
			if (other.DateOfJoining != null)
				return false;
		} else if (!DateOfJoining.equals(other.DateOfJoining))
			return false;
		if (Designation == null) {
			if (other.Designation != null)
				return false;
		} else if (!Designation.equals(other.Designation))
			return false;
		if (EmergencyContactNo == null) {
			if (other.EmergencyContactNo != null)
				return false;
		} else if (!EmergencyContactNo.equals(other.EmergencyContactNo))
			return false;
		if (EmpID == null) {
			if (other.EmpID != null)
				return false;
		} else if (!EmpID.equals(other.EmpID))
			return false;
		if (ExtensionNo == null) {
			if (other.ExtensionNo != null)
				return false;
		} else if (!ExtensionNo.equals(other.ExtensionNo))
			return false;
		if (FatherName == null) {
			if (other.FatherName != null)
				return false;
		} else if (!FatherName.equals(other.FatherName))
			return false;
		if (Gender == null) {
			if (other.Gender != null)
				return false;
		} else if (!Gender.equals(other.Gender))
			return false;
		if (Graduation == null) {
			if (other.Graduation != null)
				return false;
		} else if (!Graduation.equals(other.Graduation))
			return false;
		if (HRBP == null) {
			if (other.HRBP != null)
				return false;
		} else if (!HRBP.equals(other.HRBP))
			return false;
		if (MaritalStatus == null) {
			if (other.MaritalStatus != null)
				return false;
		} else if (!MaritalStatus.equals(other.MaritalStatus))
			return false;
		if (MobileNumber == null) {
			if (other.MobileNumber != null)
				return false;
		} else if (!MobileNumber.equals(other.MobileNumber))
			return false;
		if (MotherName == null) {
			if (other.MotherName != null)
				return false;
		} else if (!MotherName.equals(other.MotherName))
			return false;
		if (Others == null) {
			if (other.Others != null)
				return false;
		} else if (!Others.equals(other.Others))
			return false;
		if (PassportExpiryDate == null) {
			if (other.PassportExpiryDate != null)
				return false;
		} else if (!PassportExpiryDate.equals(other.PassportExpiryDate))
			return false;
		if (PassportIssuedDate == null) {
			if (other.PassportIssuedDate != null)
				return false;
		} else if (!PassportIssuedDate.equals(other.PassportIssuedDate))
			return false;
		if (PassportNo == null) {
			if (other.PassportNo != null)
				return false;
		} else if (!PassportNo.equals(other.PassportNo))
			return false;
		if (PermanentAddress == null) {
			if (other.PermanentAddress != null)
				return false;
		} else if (!PermanentAddress.equals(other.PermanentAddress))
			return false;
		if (PersonalEmailID == null) {
			if (other.PersonalEmailID != null)
				return false;
		} else if (!PersonalEmailID.equals(other.PersonalEmailID))
			return false;
		if (PostGraduation == null) {
			if (other.PostGraduation != null)
				return false;
		} else if (!PostGraduation.equals(other.PostGraduation))
			return false;
		if (PresentAddress == null) {
			if (other.PresentAddress != null)
				return false;
		} else if (!PresentAddress.equals(other.PresentAddress))
			return false;
		if (PreviousExperience == null) {
			if (other.PreviousExperience != null)
				return false;
		} else if (!PreviousExperience.equals(other.PreviousExperience))
			return false;
		if (SpouseName == null) {
			if (other.SpouseName != null)
				return false;
		} else if (!SpouseName.equals(other.SpouseName))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (ssoId == null) {
			if (other.ssoId != null)
				return false;
		} else if (!ssoId.equals(other.ssoId))
			return false;
		if (userProfiles == null) {
			if (other.userProfiles != null)
				return false;
		} else if (!userProfiles.equals(other.userProfiles))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", ssoId=" + ssoId + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", EmpID=" + EmpID + ", DateOfJoining=" + DateOfJoining + ", Band=" + Band
				+ ", Designation=" + Designation + ", Gender=" + Gender + ", ExtensionNo=" + ExtensionNo
				+ ", MobileNumber=" + MobileNumber + ", EmergencyContactNo=" + EmergencyContactNo + ", PersonalEmailID="
				+ PersonalEmailID + ", BloodGroup=" + BloodGroup + ", DateOfBirthRecords=" + DateOfBirthRecords
				+ ", DateOfBirthActual=" + DateOfBirthActual + ", MaritalStatus=" + MaritalStatus + ", SpouseName="
				+ SpouseName + ", FatherName=" + FatherName + ", MotherName=" + MotherName + ", AadhaarNumber="
				+ AadhaarNumber + ", PassportNo=" + PassportNo + ", PassportIssuedDate=" + PassportIssuedDate
				+ ", PassportExpiryDate=" + PassportExpiryDate + ", PreviousExperience=" + PreviousExperience
				+ ", PermanentAddress=" + PermanentAddress + ", PresentAddress=" + PresentAddress
				+ ", CurrentProjectManager=" + CurrentProjectManager + ", CurrentProject=" + CurrentProject
				+ ", CurrentReportingManager1=" + CurrentReportingManager1 + ", HRBP=" + HRBP + ", CurrentProjectLead="
				+ CurrentProjectLead + ", CurrentWorkLocation=" + CurrentWorkLocation + ", Graduation=" + Graduation
				+ ", PostGraduation=" + PostGraduation + ", Others=" + Others + ", userProfiles=" + userProfiles + "]";
	}


	
}
