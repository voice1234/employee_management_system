<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form:form method="POST" modelAttribute="user" >
<form:input type="hidden" path="id" id="id"/>
EMAIL
<form:input type="text" path="ssoId" id="ssoId"/>
EmpID
<form:input type="text" path="EmpID" id="EmpID"/>
NANE
<form:input type="text" path="firstName" id="firstName"/>
LASE\T
<form:input type="text" path="lastName" id="lastName"/>
PAS
<form:input type="hidden" path="password" id="password"/>
JION	
<form:input type="text" path="DateOfJoining" id="DateOfJoining"/>
band
<form:input type="text" path="Band" id="Band"/>
desig
<form:input type="text" path="Designation" id="Designation"/>
gender
<form:input type="text" path="Gender" id="Gender"/>
ex no
<form:input type="text" path="ExtensionNo" id="ExtensionNo"/>
MobileNumber
<form:input type="text" path="MobileNumber" id="MobileNumber"/>
EmergencyContactNo
<form:input type="text" path="EmergencyContactNo" id="EmergencyContactNo"/>
PersonalEmailID
<form:input type="text" path="PersonalEmailID" id="PersonalEmailID"/>
BloodGroup
<form:input type="text" path="BloodGroup" id="BloodGroup"/>
DateOfBirthRecords
<form:input type="text" path="DateOfBirthRecords" id="DateOfBirthRecords"/>
DateOfBirthActual
<form:input type="text" path="DateOfBirthActual" id="DateOfBirthActual"/>
MaritalStatus
<form:input type="text" path="MaritalStatus" id="MaritalStatus"/>
SpouseName
<form:input type="text" path="SpouseName" id="SpouseName"/>
FatherName
<form:input type="text" path="FatherName" id="FatherName"/>
MotherName
<form:input type="text" path="MotherName" id="MotherName"/>
AadhaarNumber
<form:input type="text" path="AadhaarNumber" id="AadhaarNumber"/>
PassportNo
<form:input type="text" path="PassportNo" id="PassportNo"/>
PassportIssuedDate
<form:input type="text" path="PassportIssuedDate" id="PassportIssuedDate"/>
PassportExpiryDate
<form:input type="text" path="PassportExpiryDate" id="PassportExpiryDate"/>
	PreviousExperience
<form:input type="text" path="PreviousExperience" id="PreviousExperience"/>
PermanentAddress
<form:input type="text" path="PermanentAddress" id="PermanentAddress"/>
PresentAddress
<form:input type="text" path="PresentAddress" id="PresentAddress"/>
CurrentProjectManager
<form:input type="text" path="CurrentProjectManager" id="CurrentProjectManager"/>
CurrentProject
<form:input type="text" path="CurrentProject" id="CurrentProject"/>
CurrentReportingManager1
<form:input type="text" path="CurrentReportingManager1" id="CurrentReportingManager1"/>
HRBP
<form:input type="text" path="HRBP" id="HRBP"/>
CurrentProjectLead
<form:input type="text" path="CurrentProjectLead" id="CurrentProjectLead"/>
CurrentWorkLocation
<form:input type="text" path="CurrentWorkLocation" id="CurrentWorkLocation"/>
Graduation
<form:input type="text" path="Graduation" id="Graduation"/>
PostGraduation
<form:input type="text" path="PostGraduation" id="PostGraduation"/>
Others
<form:input type="text" path="Others" id="Others"/>

<form:select path="userProfiles" items="${roles}" multiple="true" itemValue="id" itemLabel="type" class="form-control input-sm" />

<input type="submit" value="Register" />
	</form:form>