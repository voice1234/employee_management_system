<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head id="ContentPlaceHolderDefault_ContentPlaceHolderDefault_head">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- META Tag Macro -->
<meta http-equiv="imagetoolbar" content="false">
<meta http-equiv="X-UA-Compatible" content="IE=10">
<meta http-equiv="Content-Language" content="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="PayrollMaster.master">
<link rel="shortcut icon"
	href="https://intranet.valuelabs.com/images/vlfavicon.ico">
<link rel="icon" type="image/png"
	href="https://intranet.valuelabs.com/images/vlfavicon.png">
<title>My Profile</title>

<!--Styles-->
<link rel="stylesheet" type="text/css" href="static/css/custom.css"
	media="screen">
<link rel="stylesheet" type="text/css" href="static/css/lmshrcss.css"
	media="screen">
<link rel="stylesheet" type="text/css" href="static/css/stylesheet.css"
	media="screen">






<!--[if gt IE 8 ]> <style>*{filter:none;}</style> <![endif]-->
</head>

<body class="t pmmaster" oncontextmenu="return disableRightClick()">



	<form:form method="POST" modelAttribute="user">


		<div id="page">

			<div id="header">

				<div id="sitedescription" class="headertop">

					<div class="search">
						<div id="xsltsearch">
							<div class="xsltsearch_form">
								<label class="bold">Search : </label>
								<%-- <form:input  id="txtsearch" path="search" name="search" />
								&nbsp;
								<form:input path="search1" align="absbottom" /> --%>
							</div>
						</div>
					</div>

				</div>

				<div class="logo">

					<img src="static/css/download.jpg">
				</div>

				<div id="mainmenu" class="png">


					<ul class="nav-menu">
						<li class="first"><a
							href="https://intranet.valuelabs.com/intranet-home.aspx"
							alt="Intranet-Home">Home</a></li>
						<li></li>
						<li><a>My Services</a>
							<ul>
								<li><a href="<c:url value='/edit-user-${loggedinuser}'/>" alt="My-Profile">My Profile</a></li>
								<li><a> My Timesheet</a>
									<ul>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/my-services/mytimesheet.aspx"
											alt="Employee-Timesheet">My Timesheet</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/hr/timesheetbulkupload.aspx"
											alt="Timesheet Bulkupload">Timesheet Bulkupload</a></li>
									</ul></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/my-services/employeeskill.aspx"
									alt="Employee-Skills"> My Skills</a></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/My-Services/Leave-Tracking.aspx"
									alt="Leave-Tracking">My Leaves</a></li>
								<li><a href="https://pams.valuelabs.com/"
									alt="My Appraisal" target="_blank">My Appraisal</a></li>
								<li><a>My Payroll </a>
									<ul>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/payroll/accept-rules.aspx"
											alt="PayrollTaxDeclaration">Payroll Tax declaration </a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/my-services/bankdetails.aspx"
											alt="BankDetails">Bank Details</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/payroll/payslip.aspx"
											alt="My-PlaySlip">Pay Slip</a></li>
									</ul></li>
								<li></li>
								<li><a>Learning &amp; Development</a>
									<ul>
										<li></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/ld/training-feedback.aspx"
											alt="Training-Feedback">Training Feedback</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/ld/employee-nominee-form.aspx"
											alt="Employee-Nomination">Employee Nomination</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/ld/training-calendar.aspx"
											alt="Training-Calendar">Training Calendar</a></li>
										<li><a>Mandatory Training </a>
											<ul>
												<a
													href="https://intranet.valuelabs.com/intranet-home/informationsecurityawareness.aspx"
													alt="Information Security Awareness">Information
													Security Awareness</a>
											</ul></li>
									</ul></li>
								<li></li>
								<li><a>My Newsletter</a>
									<ul>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/valcomm.aspx"
											alt="valcomm">ValComm</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/Apogee.aspx"
											alt="Apogee">Apogee</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/qartz-journals.aspx"
											alt="qa-news-letter">Qartz Journals</a></li>
									</ul></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/my-services/requestlaptop.aspx"
									alt="New Laptop Request">New Laptop Request</a></li>
								<li></li>
							</ul></li>
						<li></li>
						<li><a>Process Management</a>
							<ul>
								<li><a href="https://vims.valuelabs.com/login"
									target="_blank">VIMS</a></li>
								<li><a
									href="http://process.valuelabs.com//all-portfolio-list/process-asset-library-pal/"
									target="_blank">Process Asset Library</a></li>
								<li><a href="http://172.27.52.204:8080/EmpSurvey/">
										Survey </a></li>
							</ul></li>
							<li></li>
						 <sec:authorize access="hasRole('ADMIN')">
						 <li><a>My Employees</a>
							<ul>
								<li><a href="<c:url value='/newuser'/>">Add Employee</a></li>
                                <li><a href="<c:url value='/list'/>">Show My Employees</a></li>
                                </ul>
						 </li>
								</sec:authorize>
						<li></li>
						<li></li>
					</ul>


					<div class="welcomectrl">


						Welcome, &nbsp; <span
							id="ContentPlaceHolderDefault_Logoff_3_lblUsername">${loggedinuser}</span>

						&nbsp;&nbsp;&nbsp; <a
							id="ContentPlaceHolderDefault_Logoff_3_btnLogout"
							href="<c:url value="/logout" />">Logout</a>
						<!--<a id="ContentPlaceHolderDefault_Logoff_3_btnLogout" href="javascript:__doPostBack(&#39;ctl00$ctl00$ctl00$ContentPlaceHolderDefault$Logoff_3$btnLogout&#39;,&#39;&#39;)">Log Out</a>-->
					</div>
				</div>

			</div>


			<div id="slider"></div>

			<div id="content">
				<div class="breadcrumb"></div>

				<div style="width: 100%">



					<div class="loaderpb" style="display: none;"></div>

					<div
						id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_updateProgress"
						style="display: none;" role="status" aria-hidden="true">

						<div
							style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #666666; opacity: 0.7;">
							<img
								id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_imgUpdateProgress"
								title="Loading ..." src="static/css//ProcessingImage.gif"
								alt="Loading ..."
								style="border-width: 0px; padding: 10px; position: fixed; top: 45%; left: 50%;">
						</div>

					</div>
					<div
						id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_addemployeedetails"
						class="form-info">
						<div class="form-coloum-set" style="margin-bottom: 0;">
							<h2>Employee Registration</h2>
							<span
								id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_lblErrorEmail"
								class="alertEmail" style="color: Red; display: block;"></span>
							<div class="form-row">
								<div class="form-coloum-four textCenter">



									<link href="static/css/styleForPopup.css"
										rel="stylesheet" type="text/css">

									<script src="static/css/scriptForPopup.js.download"
										type="text/javascript"></script>
									<div id="imageDiv">
										<div class="imagediv">
											<a href="javascript:void(0)" class="topopup"> <img
												id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_image_img"
												class="picLoad" src="static/css/default.jpeg"
												style="height: 100px; width: 105px; border-width: 0px;">
											</a> <form:input type="file" path=""
												name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$image$fuImage"
												id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_image_fuImage"
												class="fileUpload" accept="image/*"
												onchange="document.forms[0].submit();"/>
										</div>
										<div class="imagePopUp" style="display: none">
											<div id="toPopup">
												<div class="close"></div>
												<span class="ecs_tooltip">Press Esc to close <span
													class="arrow"></span></span>
												<div id="popup_content">
													<!--your content start-->
													<img alt="Press Esc" title="Press Esc to close"
														style="height: 390px; width: 690px;" id="image">

												</div>
												<!--your content end-->
											</div>

											<!--toPopup end-->
											<div class="loader"></div>
											<div id="backgroundPopup"></div>
										</div>
									</div>



								</div>
								<form:input type="hidden" path="id" id="id"/>
								<div class="form-coloum-two setLblWidth">
									<div class="form-coloum-two ">
										<div class="label">Emp ID :</div>
										<div class="input-field">
											<form:input type="text" path="EmpID" id="EmpID"/>
												
										</div>
									</div>
									<div class="form-coloum-two ">
										<div class="label">Email ID :</div>
										<div class="input-field">
											<form:input type="text" path="ssoId" id="ssoId"/>
											<span
												style="color: Red;"></span>
										</div>
									</div>
									<div
										id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_divDoj"
										class="form-coloum-two">
										<div class="label">Date Of Joining :</div>
										<div class="input-field">
										<form:input type="text" path="DateOfJoining" id="DateOfJoining"/>
										</div>
									</div>
									<div
										id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_upBand">

										<div class="form-coloum-two ">
											<div class="label">Band :</div>
											<div class="input-field">
												<form:select 	
													 path="Band" 
													 id="Band"
													class="normal select-dropdown">
													<option value="">Select</option>
													<option value="1">A1</option>
													<option value="15">A2</option>
													<option selected="selected" value="55">A3</option>
													<option value="108">B1</option>
													<option value="157">B2</option>
													<option value="190">B3</option>
													<option value="201">C1</option>
													<option value="218">C2</option>
													<option value="224">C3</option>
													<option value="245">C4</option>
													<option value="246">CT</option>
													<option value="229">D1</option>
													<option value="234">D2</option>
													<option value="240">D3</option>
													<option value="241">D4</option>
													<option value="242">D5</option>
													<option value="243">D6</option>
													<option value="247">FR</option>
													<option value="249">IS</option>
													<option value="248">TP</option>

												</form:select>
											</div>
										</div>
										<div class="form-coloum-two ">
											<div class="label">Designation :</div>
											<div class="input-field">
												<form:select
													path="Designation" 
													id="Designation"
													class="normal select-dropdown">
													<option value="">Select</option>
													<option value="404">AD Administrator</option>
													<option value="968">Analyst - BackOffice I&amp;AM</option>
													<option value="295">Analyst - CMDB</option>
													<option value="752">Analyst - SOC</option>
													<option value="744">Analyst – BackOffice I&amp;AM</option>
													<option value="623">Analyst Back office</option>
													<option value="534">Analyst- Payroll</option>
													<option value="639">Assistant Consultant</option>
													<option value="1177">Assistant Manager</option>
													<option value="695">Associate - Data Analyst</option>
													<option value="55">Associate - Database Analyst</option>
													<option value="506">Associate -Business Analyst</option>
													<option value="951">Associate Analyst - Client Services</option>
													<option value="500">Associate Business Analyst</option>
													<option value="489">Associate Consultant</option>
													<option value="781">Associate Technical Consultant</option>
													<option value="288">Backup Specialist</option>
													<option value="56">Business Analyst</option>
													<option value="1149">CAD Technician</option>
													<option value="826">Consultant</option>
													<option value="885">Content Writer</option>
													<option value="1043">Data Scientist</option>
													<option value="57">Database Analyst</option>
													<option value="918">Engineer - Linux</option>
													<option value="405">EUI Packaging Specialist</option>
													<option value="1017">Executive - L&amp;D</option>
													<option value="1056">Executive – HR</option>
													<option value="852">Guest Relationship Officer</option>
													<option value="58">ITGOM Shift Lead</option>
													<option value="59">ITGOM Specialist</option>
													<option value="60">Lead - Desktop Support Group</option>
													<option value="61">Lead - Help Desk</option>
													<option value="62">Linux Administrator</option>
													<option value="992">Linux Engineer</option>
													<option value="451">Network Administrator</option>
													<option value="364">NOC Engineer</option>
													<option value="557">Production Trainer</option>
													<option value="63">Programmer Analyst</option>
													<option value="859">Project Coordinator</option>
													<option value="492">Project Cordinator</option>
													<option value="792">QA Analyst</option>
													<option value="64">Release Coordinator</option>
													<option value="65">Senior Analyst</option>
													<option value="66">Senior Analyst - Global Technology Services</option>
													<option value="517">Senior Software Engineer</option>
													<option value="518">Senior Software Engineer - QA</option>
													<option value="67">Senior Visual Designer</option>
													<option value="644">Software Engineer</option>
													<option value="1048">Software Packaging Specialist</option>
													<option value="998">Software Packing Specialist</option>
													<option value="528">Software Test Engineer</option>
													<option value="68">Specialist - AD &amp; Messaging</option>
													<option value="69">Specialist - ITGOM</option>
													<option value="70">Specialist - System Administration</option>
													<option value="395">Sr .Executive - Product Sales</option>
													<option value="327">Sr Executive - Product Sales</option>
													<option value="999">Sr Linux Engineer</option>
													<option value="1001">Sr Software Engineer - QA</option>
													<option value="71">Sr. Analyst</option>
													<option value="72">Sr. Analyst - ITMSI BackOffice I&amp;AM</option>
													<option value="864">Sr. Business Analyst</option>
													<option value="650">Sr. Customer Support Agent</option>
													<option value="1039">Sr. Data Analyst</option>
													<option value="1050">Sr. Data Scientist</option>
													<option value="73">Sr. Database Administrator</option>
													<option value="729">Sr. Database Analyst</option>
													<option value="1200">Sr. Designer</option>
													<option value="631">Sr. Desinger</option>
													<option value="301">Sr. Desktop Engineer</option>
													<option value="402">Sr. Desktop Support Engineer</option>
													<option value="944">Sr. Electrical Maintenance Engineer</option>
													<option value="1180">Sr. Engineer</option>
													<option value="656">Sr. Engineer - Networking</option>
													<option value="1123">Sr. Engineer – Facilities </option>
													<option value="1054">Sr. Engineer-ID &amp; AM</option>
													<option value="624">Sr. Engineer-Linux</option>
													<option value="647">Sr. Execuitve - L &amp; D</option>
													<option value="646">Sr. Execuitve - TAG</option>
													<option value="74">Sr. Executive</option>
													<option value="250">Sr. Executive</option>
													<option value="396">Sr. Executive - Admin</option>
													<option value="106">Sr. Executive - Administration</option>
													<option value="943">Sr. Executive - AP &amp; GL</option>
													<option value="854">Sr. Executive - Back Office</option>
													<option value="1003">Sr. Executive - BackOffice</option>
													<option value="1004">Sr. Executive - BackOffice I&amp;AM</option>
													<option value="75">Sr. Executive - Banking Practice</option>
													<option value="107">Sr. Executive - BD</option>
													<option value="942">Sr. Executive - Business Finance</option>
													<option value="76">Sr. Executive - Client Services</option>
													<option value="77">Sr. Executive - CSR</option>
													<option value="690">Sr. Executive - Customer Service</option>
													<option value="78">Sr. Executive - Customer Support</option>
													<option value="398">Sr. Executive - Data Analytics</option>
													<option value="1031">Sr. Executive - Database Administration</option>
													<option value="399">Sr. Executive - Decision Science</option>
													<option value="1005">Sr. Executive - Designer</option>
													<option value="717">Sr. Executive - Facilities</option>
													<option value="79">Sr. Executive - Finance</option>
													<option value="80">Sr. Executive - HR</option>
													<option value="668">Sr. Executive - HR Operations</option>
													<option value="400">Sr. Executive - Immigration</option>
													<option value="712">Sr. Executive - Legal</option>
													<option value="397">Sr. Executive - Market Research</option>
													<option value="1135">Sr. Executive - Market Research Analyst</option>
													<option value="81">Sr. Executive - Marketing</option>
													<option value="401">Sr. Executive - Marketing Communication</option>
													<option value="82">Sr. Executive - Payroll</option>
													<option value="83">Sr. Executive - PMG</option>
													<option value="84">Sr. Executive - Presales</option>
													<option value="1034">Sr. Executive - Quality Monitoring</option>
													<option value="707">Sr. Executive - SOC</option>
													<option value="85">Sr. Executive - TAG</option>
													<option value="394">Sr. Executive - Taxation</option>
													<option value="86">Sr. Executive - Technical Support</option>
													<option value="1216">Sr. Executive – Data Analytics</option>
													<option value="865">Sr. Executive – I&amp;AM</option>
													<option value="572">Sr. Executive payroll</option>
													<option value="520">Sr. Executive-Payroll</option>
													<option value="87">Sr. Executive-Technical Support</option>
													<option value="265">Sr. GIS Engineer</option>
													<option value="247">Sr. Graphic Designer</option>
													<option value="1116">Sr. Graphic Desinger</option>
													<option value="88">Sr. ITGOM Specialist</option>
													<option value="89">Sr. Linux Administrator</option>
													<option value="90">Sr. Network Administrator</option>
													<option value="941">Sr. Network Administrator - IT</option>
													<option value="91">Sr. NOC Engineer</option>
													<option value="767">Sr. PMG Associate</option>
													<option value="671">Sr. Process Associate</option>
													<option value="403">Sr. Release Coordinator</option>
													<option value="92">Sr. Research Analyst</option>
													<option value="93">Sr. RF Engineer</option>
													<option selected="selected" value="94">Sr. Software Engineer</option>
													<option value="95">Sr. Software Engineer</option>
													<option value="96">Sr. Software Engineer</option>
													<option value="731">Sr. Software Engineer - QA</option>
													<option value="98">Sr. Software Engineer - QC</option>
													<option value="1008">Sr. Specialist-ITGOM</option>
													<option value="598">Sr. System Admin</option>
													<option value="99">Sr. System Administrator</option>
													<option value="1157">Sr. System Administrator - Windows</option>
													<option value="100">Sr. System Engineer</option>
													<option value="101">Sr. Systems Engineer</option>
													<option value="531">Sr. Technical Communicator</option>
													<option value="597">Sr. Technical Support Engineer</option>
													<option value="365">Sr. Technical Writer</option>
													<option value="635">Sr. UI designer</option>
													<option value="560">Sr. UI Developer</option>
													<option value="266">Sr. Validation Engineer</option>
													<option value="691">Sr. Visual Designer</option>
													<option value="1108">Sr. Windows Admin</option>
													<option value="102">Sr. Windows Administrator</option>
													<option value="748">Sr.&nbsp; Visual Designer</option>
													<option value="927">Sr.Administrator</option>
													<option value="1083">Sr.Analyst - Helpdesk</option>
													<option value="1105">Sr.Associate - QC</option>
													<option value="246">Sr.Electrical Engineer</option>
													<option value="545">Sr.Engineer - QC</option>
													<option value="933">Sr.Engineer -SOC</option>
													<option value="498">Sr.Engineer- Networking</option>
													<option value="804">Sr.Engineer-Windows</option>
													<option value="1026">Sr.Executive - Back OfficeI &amp; AM</option>
													<option value="505">Sr.Executive - Marketing</option>
													<option value="610">Sr.Executive - TAG</option>
													<option value="882">Sr.Executive - Technical Support</option>
													<option value="965">Sr.NOC Analyst</option>
													<option value="606">Sr.QA Engineer</option>
													<option value="103">Sr.Software Engineer</option>
													<option value="104">Sr.Software Engineer</option>
													<option value="613">Sr.Software Engineer - QA</option>
													<option value="97">Sr.Software Engineer - QA</option>
													<option value="813">Sr.Software Engineer - Servicenow</option>
													<option value="786">Sr.System Admin</option>
													<option value="666">Sr.System Engineer - Storage</option>
													<option value="1186">Sr.Systems Engineer</option>
													<option value="1035">Sr.Systems Engineer - Service Now</option>
													<option value="1063">Sr.Test Engineer</option>
													<option value="296">System Administrator</option>
													<option value="1118">System Specialist</option>
													<option value="442">Systems Engineer</option>
													<option value="910">Technical Communicator</option>
													<option value="458">UI Developer</option>
													<option value="917">Validation Specialist</option>
													<option value="1129">Web Developer</option>
													<option value="922">Windows Administrator</option>

												</form:select>
											</div>
										</div>

									</div>
								</div>
								<div class="form-coloum-two setLblWidth">
									<div class="form-coloum-two ">
										<div class="label">First Name :</div>
										<div class="input-field">
											<form:input type="text" path="firstName" id="firstName"/>
										</div>
									</div>
									<div class="form-coloum-two ">
										<div class="label nomandtory">Last Name :</div>
										<div class="input-field">
											<form:input type="text" path="lastName" id="lastName"/>
										<form:input type="hidden" path="password" id="password"/>
										</div>
									</div>
									<div class="form-coloum-two ">
										<div class="label nomandtory">Middle Name :</div>
										<div class="input-field">
											<form:input path=""
												name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$txtEmpMiddleName"
												type="text"
												id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_txtEmpMiddleName"/>
										</div>
									</div>
									<div class="form-coloum-two ">
										<div class="label">Gender :</div>
										<div class="input-field">
											<table
												id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_rdoGender"
												class="radiobuttons" cellspacing="0" cellpadding="0"
												border="0" style="border-collapse: collapse;">
												<tbody>
													<tr>
														<td><form:input type="text" path="Gender" id="Gender"/></td>
														<td></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="form-coloum-two">
										<div class="label nomandtory">Extension No :</div>
										<div class="input-field">
											<form:input type="text" path="ExtensionNo" id="ExtensionNo"/>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="tabs-wrapper">
							<ul class="payroll-tabs">
								<li
									id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_personalTab"
									class="active"><a
									href="https://intranet.valuelabs.com/intranet-home/My-Services/My-Profile.aspx#tab-1">Personal
										Information</a></li>
								<!--<li>
		<a href="#tab-2">Qualifications</a>
	</li>-->
								<li
									id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_skillTab"
									style="display: none;"><a
									href="https://intranet.valuelabs.com/intranet-home/My-Services/My-Profile.aspx#tab-2">Skill
										Matrix</a></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/My-Services/My-Profile.aspx#tab-3">Visa
										Details</a></li>

							</ul>
							<div class="tabs-content">
								<div id="tab-1" class="tab-item" style="display: block;">
									<div class="form-row">
										<div class="form-coloum-two">
											<div class="label">Mobile Number :</div>
											<div class="input-field">
												<form:input type="text" path="MobileNumber" id="MobileNumber"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label">Emergency Contact No :</div>
				<div class="input-field">
					<form:input type="text" path="EmergencyContactNo" id="EmergencyContactNo"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label">Personal Email ID :</div>
				<div class="input-field">
					<form:input type="text" path="PersonalEmailID" id="PersonalEmailID"/>
					<span
						id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_lblAlertPersonalEmail"
						style="color: Red;"></span>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label">Blood Group :</div>
				<div class="input-field">
					<form:select
						path="BloodGroup"
						id="BloodGroup"
						class="select-dropdown">
						<option value="">Select</option>
						<option value="2">A-</option>
						<option value="9">A+</option>
						<option value="8">AB-</option>
						<option value="7">AB+</option>
						<option value="4">B-</option>
						<option selected="selected" value="1">B+</option>
						<option value="6">O-</option>
						<option value="5">O+</option>

					</form:select>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label">
					Date Of Birth <i>(As per Records)</i> :
				</div>
				<div class="input-field">
				<form:input type="text" path="DateOfBirthRecords" id="DateOfBirthRecords"/>
				
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">
					Date Of Birth <i>(Actual)</i> :
				</div>
				<div class="input-field">
				<form:input type="text" path="DateOfBirthActual" id="DateOfBirthActual"/>
				</div>
			</div>
			<div
				id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_upMaritalStatus">

				<div class="form-coloum-two">
					<div class="label">Marital Status :</div>
					<div class="input-field">
						<form:select
							path="MaritalStatus"
							id="MaritalStatus"
							class="normal select-dropdown">
							<option value="0">Select</option>
							<option value="Single">Single</option>
							<option selected="selected" value="Married">Married</option>

						</form:select>
					</div>
				</div>
				<div
					id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_divSpouseName"
					class="form-coloum-two">
					<div class="label nomandtory">Spouse Name :</div>
					<div class="input-field">
						<form:input type="text" path="SpouseName" id="SpouseName"/>
					</div>
				</div>

			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Father Name :</div>
				<div class="input-field">
					<form:input type="text" path="FatherName" id="FatherName"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Mother Name :</div>
				<div class="input-field">
					<form:input type="text" path="MotherName" id="MotherName"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Aadhaar Number :</div>
				<div class="input-field">
					<form:input type="text" path="AadhaarNumber" id="AadhaarNumber"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">WorkStationNo :</div>
				<div class="input-field">
					<%-- <form:input type="text" path="WorkStationNo" id="WorkStationNo"/> --%>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Passport No :</div>
				<div class="input-field">
					<form:input type="text" path="PassportNo" id="PassportNo"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Passport Issued Date :</div>
				<div class="input-field">
				<form:input type="text" path="PassportIssuedDate" id="PassportIssuedDate"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Passport Expiry Date :</div>
				<div class="input-field">
				<form:input type="text" path="PassportExpiryDate" id="PassportExpiryDate"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Previous Experience :</div>
				<div class="input-field">
					<form:select
						path="PreviousExperience" 
						id="PreviousExperience"
						class="drop-down-small">
						<option value="0">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option selected="selected" value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
						<option value="45">45</option>
						<option value="45">45</option>
						<option value="46">46</option>
						<option value="47">47</option>
						<option value="48">48</option>
						<option value="49">49</option>
						<option value="50">50</option>
						<option value="51">51</option>
						<option value="52">52</option>
						<option value="53">53</option>
						<option value="54">54</option>
						<option value="55">55</option>
						<option value="56">56</option>
						<option value="57">57</option>
						<option value="58">58</option>
						<option value="59">59</option>
						<option value="60">60</option>
					</form:select> 
					<%-- <form:select
						name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$ddlExpMonths"
						id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_ddlExpMonths"
						class="drop-down-small">
						<option value="0">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option selected="selected" value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>

					</form:select> --%> <br> (Years) &nbsp;&nbsp; (Months)
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label">Permanent Address :</div>
				<div class="input-field">
					<form:textarea
						name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$txtPermanentAddress"
						rows="2" cols="20"
						path="PermanentAddress"
						id="PermanentAddress"
						class="text-Field"></form:textarea>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Present Address :</div>
				<div class="input-field">
					<form:textarea
						name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$txtPresentAddress"
						rows="2" cols="20"
						path="PresentAddress" id="PresentAddress"
						class="text-Fields"></form:textarea>
				</div>
			</div>
			<div
				id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_update">

				<div class="form-row">
					<div class="form-coloum-two">
						<div class="label nomandtory">Current Project Manager :</div>
						<div class="input-field">

<form:input type="text" path="CurrentProjectManager" id="CurrentProjectManager"/>
						</div>
					</div>
					<div class="form-coloum-two">
						<div class="label nomandtory">Current Project :</div>
						<div class="input-field">

<form:input type="text" path="CurrentProject" id="CurrentProject"/>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="form-coloum-two">
					<div class="label nomandtory">Current Reporting Manager1 :</div>
					<div class="input-field">


						<form:input type="text" path="CurrentReportingManager1" id="CurrentReportingManager1"/>
					</div>
				</div>
				<div class="form-coloum-two">
					<div class="label nomandtory">HRBP :</div>
					<div class="input-field">
<form:input type="text" path="HRBP" id="HRBP"/>
					</div>
				</div>

			</div>
			<div class="form-coloum-two">
				<div class="label nomandtory">Current Project Lead :</div>
				<div class="input-field">
					<form:input type="text" path="CurrentProjectLead" id="CurrentProjectLead"/>
				</div>
			</div>
			<div class="form-coloum-two">
				<div class="label">Current Work Location :</div>
				<div class="input-field">
					<form:select
						name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$ddlCuurentWorkLocation"
						path="CurrentWorkLocation"
						id="CurrentWorkLocation"
						class="normal select-dropdown">
						<option value="">Select</option>
						<option value="27">Anantapur / ValueLabs LLP</option>
						<option value="14">Australia / Valuelabs Australia Pty Ltd.</option>
						<option value="8">Bengaluru / ValueLabs Solutions LLP</option>
						<option value="22">Canada / Valuelabs Solutions Inc</option>
						<option value="10">Chennai / ValueLabs LLP</option>
						<option value="36">Default / Default</option>
						<option value="41">Dubai / ValueLabs LLP</option>
						<option value="43">Dubai / VL Consulting FZ LLC.</option>
						<option value="38">HYD-Main Building / ValueLabs LLP</option>
						<option value="39">Hyderabad / A H Digital Media Pvt. Ltd</option>
						<option value="1">Hyderabad / ValueLabs Corporate Office</option>
						<option value="44">Hyderabad / ValueLabs Infra LLP</option>
						<option value="40">Hyderabad / ValueLabs LLP</option>
						<option value="35">Hyderabad / ValueLabs Services Pvt. Ltd.</option>
						<option value="45">Hyderabad / ValueLabs Services Pvt. Ltd.</option>
						<option value="46">Hyderabad / ValueLabs Services Pvt. Ltd.</option>
						<option selected="selected" value="2">Hyderabad / ValueLabs SEZ</option>
						<option value="6">Hyderabad / ValueLabs Solutions LLP</option>
						<option value="30">Hyderabad / ValueLabs Solutions LLP (Unit 2)</option>
						<option value="3">Hyderabad / ValueLabs Technologies LLP</option>
						<option value="4">Hyderabad / ValueLabs Technologies-II</option>
						<option value="5">Hyderabad / ValueLabs Technologies-III</option>
						<option value="7">Indore / ValueLabs LLP</option>
						<option value="12">Kolkata / ValueLabs LLP</option>
						<option value="29">Mahabub Nagar / ValueLabs LLP</option>
						<option value="23">Malaysia / ValueLabs SDN BHD</option>
						<option value="11">Mumbai / ValueLabs LLP</option>
						<option value="32">Nagpur / ValueLabs LLP</option>
						<option value="21">Nethrlands / VLIT Services B.V.</option>
						<option value="31">Nizamabad / ValueLabs LLP</option>
						<option value="9">Noida / ValueLabs LLP</option>
						<option value="17">Philippines / ValueLabs (Philippines) Inc.</option>
						<option value="16">Singapore / ValueLabs Global Solutions Pte Ltd</option>
						<option value="20">Sweden / ValueLabs AB</option>
						<option value="13">UAE / ValueLabs FZ - LLC</option>
						<option value="47">UAE / Valuelabs LLP Dubai Branch.</option>
						<option value="49">UAE / VLC DWC LLC</option>
						<option value="18">UK / ValueLabs (UK) Limited</option>
						<option value="42">US / A.H. TECHNOLOGIES LLC.</option>
						<option value="19">USA / ValueLabs, Inc.</option>

					</form:select>
				</div>
			</div>

			<div class="clear"></div>
		</div>




		<div class="form-row">
			<div class="form-coloum-two">
				<span
					id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_lblQualification"
					class="label">Graduation :</span>
				<div class="input-field">
					<form:select
						name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$ddlGraduation"
						path="Graduation" id="Graduation"
						class="select-dropdown">
						<option value="">Select</option>
						<option value="1">AMIE</option>
						<option value="5">B. Tech</option>
						<option selected="selected" value="6">B. Tech (CSE)</option>
						<option value="7">B. Tech (ECE)</option>
						<option value="8">B. Tech (EEE)</option>
						<option value="9">B. Tech (IT)</option>
						<option value="10">B. Tech (ME)</option>
						<option value="12">B.A</option>
						<option value="13">B.B.M</option>
						<option value="14">B.C.A</option>
						<option value="15">B.Com</option>
						<option value="16">B.Design</option>
						<option value="17">B.E</option>
						<option value="19">B.F.A</option>
						<option value="20">B.M.S</option>
						<option value="22">B.Pharmacy</option>
						<option value="23">B.Sc</option>
						<option value="24">B.Sc Computers</option>
						<option value="27">BA</option>
						<option value="29">BBM</option>
						<option value="30">BCA</option>
						<option value="33">BE (ECE)</option>
						<option value="34">BE (Production)</option>
						<option value="39">BEd</option>
						<option value="40">BFA</option>
						<option value="42">Bio Tech</option>
						<option value="43">BIT</option>
						<option value="46">CFA</option>
						<option value="47">DAC</option>
						<option value="48">DBM</option>
						<option value="49">DCA</option>
						<option value="50">DECE</option>
						<option value="51">DEE</option>
						<option value="55">Diploma ECE</option>
						<option value="56">Diploma in CSC</option>
						<option value="57">Diploma in EC</option>
						<option value="58">Diploma in Hotel Management</option>
						<option value="59">Diploma in Multimedia</option>
						<option value="60">DME</option>
						<option value="61">DME, A- Level</option>
						<option value="62">DOEACC, B Level</option>
						<option value="63">EPBM</option>
						<option value="67">I.B</option>
						<option value="69">BHMS</option>
						<option value="141">Graduation</option>
						<option value="143">NA</option>

					</form:select>
				</div>
			</div>
			<div class="form-coloum-two">
				<span
					
					class="label nomandtory">Post Graduation :</span>
				<div class="input-field">
					<form:select
					path="PostGraduation" id="PostGraduation"
						name="ctl00$ctl00$ctl00$ContentPlaceHolderDefault$leftcontent$UC.Valuelabs.ERP.HRSelfEdit_7$ddlPG">
						<option value="">Select</option>
						<option value="72">M.A</option>
						<option value="73">M.B.A</option>
						<option value="74">M.C.A</option>
						<option value="75">M.E</option>
						<option value="76">M.I.T</option>
						<option value="77">M.Phil</option>
						<option value="78">M.Phill</option>
						<option value="81">M.Sc </option>
						<option value="82">M.Sc Electronics</option>
						<option value="83">M.Sc(Physics)</option>
						<option value="84">M.Sc-IT</option>
						<option value="85">M.Tech</option>
						<option value="86">MA</option>
						<option value="87">MBBS</option>
						<option value="88">MBA</option>
						<option value="92">MCA</option>
						<option value="94">MCIS</option>
						<option value="95">MCM</option>
						<option value="96">MCom</option>
						<option value="97">MCP</option>
						<option value="98">MCS</option>
						<option value="99">ME</option>
						<option value="100">MEC</option>
						<option value="101">MFM</option>
						<option value="102">MHRM</option>
						<option value="103">MISCA</option>
						<option value="104">MMS</option>
						<option value="106">MS</option>
						<option value="107">MS (CIS)</option>
						<option value="108">MS (CS)</option>
						<option value="109">MSc</option>
						<option value="110">Msc (CS)</option>
						<option value="111">Msc (IS)</option>
						<option value="112">MSc (IT)</option>
						<option value="113">MSc in HRM, BE</option>
						<option value="114">MSC IT</option>
						<option value="115">MSIT</option>
						<option value="116">MSW</option>
						<option value="117">P.G.D.M- HR</option>
						<option value="118">PG</option>
						<option value="119">PG Diploma</option>
						<option value="121">PGDBA</option>
						<option value="123">PGDBI</option>
						<option value="124">PGDBM</option>
						<option value="125">PGDCA</option>
						<option value="126">PGDCS</option>
						<option value="127">PGDIEM</option>
						<option value="128">PGDITM</option>
						<option value="129">PGDM</option>
						<option value="131">PGD-MISCA</option>
						<option value="132">PGOM</option>
						<option value="135">Master of Technology-M.Tech (Civil)</option>
						<option value="140">Chartered Accountant</option>
						<option value="142">Post Graduation</option>
						<option value="144">Not Available</option>

					</form:select>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="form-row">
			<div class="form-coloum-two">
				<span
					id="ContentPlaceHolderDefault_leftcontent_UC.Valuelabs.ERP.HRSelfEdit_7_lblOthers"
					class="label  nomandtory">Others :</span>
				<div class="input-field">
					<form:input type="text" path="Others" id="Others"/>
>
				</div>
					<div class="input-field">
				
				</div>
			</div>
			<div class="clear">
			<form:select path="userProfiles" items="${roles}" multiple="true" itemValue="id" itemLabel="type"></form:select>
			<input type="submit" value="Register" />
			</div>
		</div>
		<div
			style="background: #f2f2f2; font-size: 11px; font-style: italic; margin: 5px 0; padding: 5px;">
			Note : For any further clarifications please contact your respective
			HRBP</div>
		</div>
		<div id="tab-2" class="tab-item" style="display: none;">
			
			
		</div>
			


	</form:form>




</body>
</html>

