<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head id="ContentPlaceHolderDefault_head">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- META Tag Macro -->
<meta http-equiv="imagetoolbar" content="false">

<!--<meta http-equiv="X-UA-Compatible" content="IE=Edge"> -->
<meta http-equiv="Content-Language" content="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="VLIntranetMaster.master">

<link rel="shortcut icon"
	href="https://intranet.valuelabs.com/images/vlfavicon.ico">
<link rel="icon" type="image/png"
	href="https://intranet.valuelabs.com/images/vlfavicon.png">

<title>Home</title>

<!--Styles-->
<link rel="stylesheet" type="text/css" href="static/css/custom.css"
	media="screen">
<link rel="stylesheet" type="text/css" href="static/css/lmshrcss.css"
	media="screen">
<link rel="stylesheet" type="text/css" href="static/css/stylesheet.css"
	media="screen">
</head>

<body class="t vlmaster" oncontextmenu="return disableRightClick()">
	<form method="post"
		action="https://intranet.valuelabs.com/intranet-home.aspx" id="form1">


		<div id="page">

			<div id="header">

				<div id="sitedescription" class="headertop">

					<div class="search">
						<div id="xsltsearch">
							<div class="xsltsearch_form">
								<label class="bold">Search : </label><input id="txtsearch"
									name="search"
									onkeyup="if(event.keyCode ==13)window.location.href=&#39;/intranet-home/result.aspx?search=&#39;+search.value; "
									type="text" autocomplete="off" class="input srhbtn" value="">&nbsp;<input
									align="absbottom" type="image"
									src="./Home_files/search-icon.gif" title="Search"
									onclick="window.location.href=&#39;/intranet-home/result.aspx?search=&#39;+search.value;return false;"
									value="Search">
							</div>
						</div>
					</div>

				</div>

				<div class="logo">
					<img src="static/css/download.jpg">
				</div>

				<div id="mainmenu" class="png">


					<ul class="nav-menu">
						<li class="first"><a
							href="https://intranet.valuelabs.com/intranet-home.aspx"
							alt="Intranet-Home">Home</a></li>
						<li></li>
						<li><a>My Services</a>
							<ul>
								<li><a href="/list" alt="My-Profile">My Profile</a></li>
								<li><a> My Timesheet</a>
									<ul>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/my-services/mytimesheet.aspx"
											alt="Employee-Timesheet">My Timesheet</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/hr/timesheetbulkupload.aspx"
											alt="Timesheet Bulkupload">Timesheet Bulkupload</a></li>
									</ul></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/my-services/employeeskill.aspx"
									alt="Employee-Skills"> My Skills</a></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/My-Services/Leave-Tracking.aspx"
									alt="Leave-Tracking">My Leaves</a></li>
								<li><a href="https://pams.valuelabs.com/"
									alt="My Appraisal" target="_blank">My Appraisal</a></li>
								<li><a>My Payroll </a>
									<ul>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/payroll/accept-rules.aspx"
											alt="PayrollTaxDeclaration">Payroll Tax declaration </a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/my-services/bankdetails.aspx"
											alt="BankDetails">Bank Details</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/payroll/payslip.aspx"
											alt="My-PlaySlip">Pay Slip</a></li>
									</ul></li>
								<li></li>
								<li><a>Learning &amp; Development</a>
									<ul>
										<li></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/ld/training-feedback.aspx"
											alt="Training-Feedback">Training Feedback</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/ld/employee-nominee-form.aspx"
											alt="Employee-Nomination">Employee Nomination</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/ld/training-calendar.aspx"
											alt="Training-Calendar">Training Calendar</a></li>
										<li><a>Mandatory Training </a>
											<ul>
												<a
													href="https://intranet.valuelabs.com/intranet-home/informationsecurityawareness.aspx"
													alt="Information Security Awareness">Information
													Security Awareness</a>
											</ul></li>
									</ul></li>
								<li></li>
								<li><a>My Newsletter</a>
									<ul>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/valcomm.aspx"
											alt="valcomm">ValComm</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/Apogee.aspx"
											alt="Apogee">Apogee</a></li>
										<li><a
											href="https://intranet.valuelabs.com/intranet-home/qartz-journals.aspx"
											alt="qa-news-letter">Qartz Journals</a></li>
									</ul></li>
								<li><a
									href="https://intranet.valuelabs.com/intranet-home/my-services/requestlaptop.aspx"
									alt="New Laptop Request">New Laptop Request</a></li>
								<li></li>
							</ul></li>
						<li></li>
						<li><a>Process Management</a>
							<ul>
								<li><a href="https://vims.valuelabs.com/login"
									target="_blank">VIMS</a></li>
								<li><a
									href="http://process.valuelabs.com//all-portfolio-list/process-asset-library-pal/"
									target="_blank">Process Asset Library</a></li>
								<li><a href="http://172.27.52.204:8080/EmpSurvey/">
										Survey </a></li>
							</ul></li>
							<li></li>
						 <sec:authorize access="hasRole('ADMIN')">
						 <li><a href="<c:url value='/list'/>">Employee List</a></li>
								</sec:authorize>
						<li></li>
						<li></li>
					</ul>


					<div class="welcomectrl">


						Welcome, &nbsp; <span
							id="ContentPlaceHolderDefault_Logoff_3_lblUsername">${loggedinuser}</span>

						&nbsp;&nbsp;&nbsp; <a
							id="ContentPlaceHolderDefault_Logoff_3_btnLogout"
							href="<c:url value="/logout" />">Logout</a>
						<!--<a id="ContentPlaceHolderDefault_Logoff_3_btnLogout" href="javascript:__doPostBack(&#39;ctl00$ctl00$ctl00$ContentPlaceHolderDefault$Logoff_3$btnLogout&#39;,&#39;&#39;)">Log Out</a>-->
					</div>
				</div>









			</div>


			<div id="slider">

				<div id="imageSlider">
					<img src="./Home_files/solutions-header.jpg">


				</div>


			</div>

			<div id="content">
				<div class="breadcrumb"></div>


				<div id="leftcontent">


					<script type="text/javascript">
      function popup(){
      var val=document.getElementById('selectlinks').value;
      if(val)var winPop = window.open(val);
      }
    </script>
					<div class="hotspot">
						<h3>Quick Links</h3>
						<ul>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/code-of-business-ethics-v2.aspx"
								title="Code of Business Ethics and Conduct Policy"
								target="_blank"> Code of Business Ethics and Conduct Policy<span
									class="blink_me">&nbsp; *New</span></a></li>
							<li><a
								href="https://intranet.valuelabs.com/media/1817175/india-holiday-list-2017-3-.pdf"
								title="Holiday List - 2017" target="_blank"> Holiday List -
									2017<span class="blink_me">&nbsp; *New</span>
							</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/my-services/employeeholidaylist.aspx"
								title="Employee Holiday List"> Employee Holiday List<span
									class="blink_me">&nbsp; *New</span></a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/it-security.aspx"
								title="IT AND SECURITY POLICIES" target="_blank"> IT And
									Security Policy Video<span class="blink_me">&nbsp; *New</span>
							</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/jogw.aspx"
								title="Meet Joy&#39;s" target="_blank"> Meet Joy's </a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/joy-of-giving-week.aspx"
								title="Joy of giving week" target="_blank"> Joy of giving
									week </a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/dress-code-policy.aspx"
								title="Dress Code" target="_blank"> Dress Code </a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/team-outing.aspx"
								title="Team Outing" target="_blank">Team Outing</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/whistleblower-policy.aspx"
								title="whistleblower-policy" target="_blank">Whistleblower</a></li>
							<li><a
								href="https://intranet.valuelabs.com/media/1430665/Yammer-User-Guide.pdf"
								title="Yammer User Guide" target="_blank">Yammer Guide</a></li>
							<li><a href="https://portal.office.com/"
								title="Yammer Login" target="_blank">Yammer Login</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/vltv-home/vl-tv-news-june-17-2016.aspx"
								target="_blank" title="VLTV"> VLTV </a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/inspire-2015-news/inspire_2015_venue_video.aspx"
								title="Inspire News" target="_blank"> Inspire News </a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/chicagoinspire2015video-arjunsspeech.aspx"
								title="Inspire 2015 Videos" target="_blank">Inspire 2015
									Videos</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/chicagoinspire2015presentations.aspx"
								title="Inspire 2015 Presentations" target="_blank">Inspire
									2015 Presentations</a></li>
							<li><a
								href="https://intranet.valuelabs.com/media/1051/handbook-vl_final-forvims.pdf"
								title="Employee Hand Book" target="_blank">Employee Hand
									Book</a></li>
							<li><a
								href="https://intranet.valuelabs.com/media/2040571/global-travel-policy-final-version.pdf"
								title="Global Travel Policy" target="_blank">Global Travel
									Policy</a></li>
							<li><a
								href="https://intranet.valuelabs.com/media/1766507/salary-advance-policy.pdf"
								title="Salary Advance Policy" target="_blank">Salary Advance
									Policy</a></li>
							<li><a href="http://www.timeanddate.com/worldclock/"
								title="World Clock – Time Zones" target="_blank">World Clock
									– Time Zones</a></li>
							<li><a href="http://youthforseva.org/"
								title="Youth for Seva Hyderabad" target="_blank">Youth for
									Seva Hyderabad</a></li>
							<li><a href="http://www.ceoandhra.nic.in/"
								title="Electoral Officer" target="_blank"> Electoral Officer</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/poshpresentation.aspx"
								title="Posh Presentation" target="_blank"> Posh Presentation</a></li>
							<li><a href="https://marketinglounge.valuelabs.com/"
								title="Marketing Lounge" target="_blank">Marketing Lounge</a></li>
							<li><a href="https://pmotracker.valuelabs.net/"
								title="PMO Tracker" target="_blank" alt="PayrollTaxDeclaration">PMO
									Tracker</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/admin/smhomepage.aspx"
								title="Space Management">Space Management</a></li>
							<li class="lastli"><select onchange="OpenWin(this.value);"
								id="selectlinks"><option value="" selected="selected">Select
										a Link</option>
									<option value="https://mx.valuelabs.net/webmail/src/login.php">mx.valuelabs.net</option>
									<option value="http://mail.valuelabs.com/">mail.valuelabs.com</option>
									<option value="/intranet-home/addsignature.aspx">Email
										Signature</option>
									<option value="/intranet-home/MySignatureAdd.aspx">My
										Email Signature</option>
									<option value="/intranet-home/suggestionboxnewpost.aspx">Suggestion
										Box</option></select>
								<p>
									<button type="button" class="inputbtn" id="btnSelect"
										onclick="popup();">Go</button>
								</p></li>
						</ul>
					</div>
					<div class="hotspot">
						<h3>
							Employee<br>Self-Service
						</h3>
						<ul>
							<li><a href="https://portal.myvantageconnect.co.in/#/Home"
								target="_blank">Mediclaim eCard</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/valuelabssurvery.aspx"
								target="_blank">Fun @ Work</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/health-checkups/heart-health-package-@-rs1999-yashoda-hospital.aspx"
								target="_self"> Health Checkup<span class="blink_me">&nbsp;
										*New</span></a></li>
						</ul>
					</div>


				</div>


				<div class="middlecontent">



					<a
						href="https://intranet.valuelabs.com/intranet-home/vltv-home/vl-tv-news-june-17-2016.aspx">
						<img src="./Home_files/VLtv-News-June17-2016.jpg" width="570px">
					</a>



				</div>


				<div id="rightcontent">

					<div class="hotspot">
						<h3>Clever Tools</h3>
						<ul class="nobgicons">
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/travelagent.aspx"><img
									src="./Home_files/icon_app_travel.gif"> Travel Agent</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/acronymmanager.aspx"><img
									src="./Home_files/icon_app_acronym.gif"> Acronym Manager
							</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/companycalendar.aspx"><img
									src="./Home_files/icon_app_calendar.gif"> Company
									Calendar</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/documentmanager.aspx"><img
									src="./Home_files/icon_app_documentmanager.gif"> Document
									Manager</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/fileexpress.aspx"><img
									src="./Home_files/icon_app_ferry.gif"> File Express</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/forum.aspx"><img
									src="./Home_files/icon_app_forum.gif"> Forum</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/staffoffershome.aspx"><img
									src="./Home_files/icon_app_staffoffers.gif"> Staff Offers</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/meetingroombooking.aspx"><img
									src="./Home_files/icon_app_meetingroom.gif"> Meeting Room
									Booking</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/news-homepage.aspx"><img
									src="./Home_files/icon_app_news.gif"> News</a></li>
							<li><a
								href="https://intranet.valuelabs.com/intranet-home/pmg/marketingmaterial.aspx"
								title="MarketingMaterial"><img width="20" height="20"
									src="./Home_files/icon_20x20.jpg" alt="Image Repository Icon">&nbsp;Image
									Repository</a></li>
							<li><b>More Clever Tools</b><br> <br> <select
								onchange="if(this.value.length &gt; 0) window.open(&#39;&#39;,&#39;_self&#39;).location.href=this.value"><option
										value="">Select a Tool</option>
									<option value="/intranet-home/feedbackdirector.aspx">Feedback
										Director</option></select></li>
						</ul>
					</div>

				</div>


				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div id="footer">

				<div class="footwrp">
					<div id="footer-end">
						Powered by <a href="http://www.valuelabs.com/" title="ValueLabs">ValueLabs</a>
					</div>
					<div id="footercontact">
						<span>Contact information:</span> ValueLabs,41 Hitech City.
						Madhapur-Hyd 500081. Telephone: +040 66239000
					</div>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>

		<div>

			<input type="hidden" name="__VIEWSTATEGENERATOR"
				id="__VIEWSTATEGENERATOR" value="CA0B0334">
		</div>
	</form>



</body>
</html>
